Les fichiers de sorties de Regartercept sont d'une part un fichier 
d'historique dans /var/log/regartercept/regartercept.log, texte lisible 
par un humain, écrit sur le disque dur, et d'une autre, les valeurs 
numériques (néanmoins codées en ASCII), écrites dans un ramdisk par 
défaut dans "/dev/shm/gasesmonitoring/<numéro de voie>.<extension>". 
Il y a trois fichiers 
par canal :

* XXXXX.infos :

Ce fichier est généré par regartercept à son lancement à partir du 
fichier Channels.csv récupéré sur la tablette Dräger (attention : ce 
fichier doit être converti en UTF-8) et contient les infos suivantes :

	enabled (1 ou plus mais si 0, aucun fichier ne sera généré)
	group (chaine)
	name (chaine)
	gas (chaine)
	unit (chaine)
	
* XXXXX.value :

Ce fichier contient deux lignes, la première étant la valeur la plus 
récente que nous avons vu passer sur le bus reliant la tablette à la 
carte "gate". La seconde étant une valeur mémorisée comme "pire" pour ne
pas louper de valeur de crête entre deux relevés de points.

	valeur récente (20.1)
	valeur mémorisée (18.4)
	
* XXXXX.alarm :

Valeur de l'état de l'alarme la plus récente vu sur le bus sur 16 bits 
sur la première ligne, chaque bit représentant un flag. La seconde ligne 
indique si le capteur est repéré par regartercept comme défectueux 
(flags "ACTIVE" et "INHIBIT" qui apparaissent en même temps).

	valeur d'alarme (entier sur 16 bits)
	indication capteur défectueux (1 ou 0)

La signification des différents bits est selon la documentation Dräger 
et est la suivante, du bit le moins significatif au plus significatif :

	"A1 UN-ACK",
	"A1 ACTIVE",
	"A1 TRIPPED",
	"CRITICAL FAULT",
	"A2 UN-ACK",
	"A2 ACTIVE",
	"A2 TRIPPED",
	"HCAL",
	"A3 UN-ACK",
	"A3 ACTIVE",
	"A3 TRIPPED",
	"INHIBIT",
	"UN-ACK",
	"ACTIVE",
	"TRIPPED",
	"NORESPONSE"


________________________________________________________________________
                                             Romain JOORIS
                                             19 Mars 2015 (v001)
